"use strict";

const path = require('path');

const __root = path.join(__dirname, '..');
const Jimp = require('jimp');

console.log(__root);

const dirs = {
    originals: path.join(__root, 'originals'),
    optimized: path.join(__root, 'optimized'),
    originals_cropped: path.join(__root, 'cropped')
};

console.log(dirs);

const fs = require('fs');

const files = fs.readdirSync(dirs.originals);

const resizes = [];

let fileNext = function(){
    if(files.length > 0){
        let fileName = files.shift();
        let file = path.join(dirs.originals, fileName);
        let fileOut = path.join(dirs.originals_cropped, fileName);

        console.log(fileName);
        console.log(fileOut);

        if(fs.existsSync(fileOut)){
            console.log('Cached: '+fileName);
            fileNext();
        }else
            Jimp.read(file, function(err, img){
                 let bounds = {
                     x: { low: false, high: false },
                     y: { low: false, high: false }
                 };
                 img.scan(0, 0, img.bitmap.width, img.bitmap.height, function(x,y,idx){
                     if(this.bitmap.data[idx + 3] > 20){
                         if(!bounds.x.low || bounds.x.low > x) bounds.x.low = x;
                         if(!bounds.y.low || bounds.y.low > y) bounds.y.low = y;

                         if(!bounds.x.high || bounds.x.high < x) bounds.x.high = x;
                         if(!bounds.y.high || bounds.y.high < y) bounds.y.high = y;
                     }
                 }, function(){
                     let width = bounds.x.high - bounds.x.low;
                     let height = bounds.y.high - bounds.y.low;

                     img
                         .crop(bounds.x.low, bounds.y.low, width, height)
                         .write(fileOut);

                     console.log('Finished: '+fileName);
                     fileNext();
                 });
            });
    }else{
        console.log('done!');
        process.exit(0);
    }
};

fileNext();

for(let i=0;i<files.length;i++){
}

// console.log(files);